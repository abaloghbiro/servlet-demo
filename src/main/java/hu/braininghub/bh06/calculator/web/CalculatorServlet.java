package hu.braininghub.bh06.calculator.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hu.braininghub.bh06.calculator.Calculator;
import hu.braininghub.bh06.calculator.Operation;

@WebServlet(urlPatterns = "/calculator")
public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String op1 = req.getParameter("op1");
		String op2 = req.getParameter("op2");
		String operator = req.getParameter("operator");

		boolean validationResult = true;

		if (op1 == null) {
			resp.getWriter().println("Missing operand op1!");
			validationResult = false;
		}
		if (op2 == null) {
			resp.getWriter().println("Missing operand op2!");
			validationResult = false;
		}
		if (operator == null) {
			resp.getWriter().println("Missing operator!");
			validationResult = false;
		}

		if (!validationResult) {
			resp.setStatus(400);
			return;
		}

		Double ret = Calculator.calculate(Operation.valueOf(operator), Double.parseDouble(op1),
				Double.parseDouble(op2));

		resp.getWriter().println("<h1>Result: " + ret + "</h1>");

	}

}
