package hu.braininghub.bh06.calculator;

public class MinusOperation implements CalculationOperation<Double, Double> {

	@Override
	public Double calculate(Double op1, Double op2) {

		if (op1 == null || op2 == null) {
			throw new IllegalArgumentException("Missing operand!");
		}

		return op1 - op2;
	}

	@Override
	public Operation getOperationType() {
		return Operation.SUB;
	}
}
