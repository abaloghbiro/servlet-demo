package hu.braininghub.bh06.calculator;

import java.util.ArrayList;
import java.util.List;

public class Calculator {

	private static final List<CalculationOperation<?, ?>> OPERATIONS = new ArrayList<>();

	static {
		OPERATIONS.add(new AddOperation());
		OPERATIONS.add(new MinusOperation());
		OPERATIONS.add(new MultiplyOperation());
		OPERATIONS.add(new DivisionOperation());
	}

	public static <T extends Number> T calculate(Operation op, T op1, T op2) {

		@SuppressWarnings("unchecked")
		CalculationOperation<T, T> operation = (CalculationOperation<T, T>) OPERATIONS.stream()
				.filter(o -> o.getOperationType() == op).findFirst().get();

		return operation.calculate(op1, op2);
	}
}
