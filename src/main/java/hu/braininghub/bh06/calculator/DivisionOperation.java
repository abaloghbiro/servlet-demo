package hu.braininghub.bh06.calculator;

public class DivisionOperation implements CalculationOperation<Double, Double> {

	@Override
	public Double calculate(Double op1, Double op2) {

		if (op1 == null || op2 == null) {
			throw new IllegalArgumentException("Missing operand!");
		}

		if (op2.intValue() == 0) {
			throw new IllegalArgumentException("Division by zero!");
		}
		
		double value = op1 / op2;
		return value;
	}
	
	@Override
	public Operation getOperationType() {
		return Operation.DIV;
	}

}
