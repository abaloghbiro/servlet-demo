package hu.braininghub.bh06.calculator;

public interface CalculationOperation<T extends Number,V extends Number> {

	V calculate(T op1, T op2);
	
	Operation getOperationType();
}
